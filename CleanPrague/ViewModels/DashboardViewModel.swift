//
//  DashboardViewModel.swift
//  CleanPrague
//
//  Created by Adam Formanek on 27.01.2021.
//

import MapKit
import Foundation
import CoreLocation
import Combine


enum DataState {
    case noData
    case fetching
    case loaded
    case filtering
}

final class DashboardViewModel: NSObject, ObservableObject, Identifiable {
    @Published private(set) var state: DataState = .noData
    @Published private(set) var trashCanGroups: [TrashCanGroup] = [] {
        didSet {
            print("updated")
        }
    }
    
    @Published var location: CLLocation = CLLocation(latitude: 51.507222, longitude: -0.1275) {
        didSet {
            print("location updated")
        }
    }
    
    private var trashBinTypes: [TrashCan] = []
    private var trashBinLocations: [TrashCan] = []
    private var trashCans: [TrashCan] = []
    
    private var userLocation = CLLocation() {
        didSet {
            DispatchQueue.main.async {
                self.location = self.userLocation
            }
        }
    }
    
    private let locationManager = CLLocationManager()
    private let networkService: NetworkServicing = NetworkService()
    
    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    
    func fetchTrashBins() {
        DispatchQueue.main.async {
            self.state = .fetching
        }
        locationManager.requestWhenInUseAuthorization()
        let group = DispatchGroup()

        group.enter()
        fetchTrashBinTypes {
            group.leave()
        }
        
        group.enter()
        fetchTrashBinLocations {
            group.leave()
        }
        
        group.notify(queue: DispatchQueue.global()) {
            self.mergeTrashCanTypeAndLocation()
        }
    }
}

// MARK: - User's Location
extension DashboardViewModel: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        if userLocation.distance(from: location) > 200 {
            self.userLocation = location
            updateTrashCanGroupsDistance()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("NOOOOOOT")
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        switch manager.authorizationStatus {
        case .notDetermined, .restricted, .denied:
            print("Man i dont have permitions, fucc...")
        case .authorizedAlways, .authorizedWhenInUse:
            manager.requestLocation()
        @unknown default:
            break
        }
    }
}

// MARK: - TrashCanLocations
extension DashboardViewModel {
    func fetchTrashBinLocations(completion: @escaping () -> Void) {
        state = .fetching
        networkService.fetch(url: .locations) { result in
            switch result {
            case .success(let data):
                guard let objects = try? MKGeoJSONDecoder().decode(data) as? [MKGeoJSONFeature] else {
                    fatalError("Incorrect JSON format.")
                }
                self.parseTrashBinLocations(objects) { trashBins in
                    self.trashBinLocations = trashBins
                    completion()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func parseTrashBinLocations(_ objects: [MKGeoJSONFeature], completion: @escaping ([TrashCan]) -> Void) {

        let trashBins: [TrashCan] = objects.compactMap { (feature) in
            guard let geometry = feature.geometry.first, let properties = feature.properties else {
                return nil
            }

            guard let annotation = geometry as? MKPointAnnotation else {
                return nil
            }
            let property = try? JSONDecoder.init().decode(TrashBinLocationProperty.self, from: properties)
            
            return TrashCan(
                trashBinLocationProperty: property,
                annotation: annotation
            )
        }
        completion(trashBins)
    }
}

// MARK: - TrashCanTypes
extension DashboardViewModel {
    func fetchTrashBinTypes(completion: @escaping () -> Void) {
        networkService.fetch(url: .types) { result in
            switch result {
            case .success(let data):
                guard let objects = try? MKGeoJSONDecoder().decode(data) as? [MKGeoJSONFeature] else {
                    fatalError("Incorrect JSON format.")
                }
                self.parseTrashBinTypes(objects) { trashBins in
                    self.trashBinTypes = trashBins
                    completion()

                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func parseTrashBinTypes(_ objects: [MKGeoJSONFeature], completion: @escaping ([TrashCan]) -> Void) {

        let trashBins: [TrashCan] = objects.compactMap { (feature) in
            guard let geometry = feature.geometry.first, let properties = feature.properties else {
                return nil
            }

            guard let annotation = geometry as? MKPointAnnotation else {
                return nil
            }
            let property = try? JSONDecoder.init().decode(TrashBinTypeProperty.self, from: properties)
            
            return TrashCan(
                trashBinTypeProperty: property,
                annotation: annotation
            )
        }
        completion(trashBins)
    }
}

extension DashboardViewModel {
    private func mergeTrashCanTypeAndLocation() {
        self.trashCans = trashBinTypes.compactMap({ trashCanType in
            let location = self.trashBinLocations.first { (trashCanLocation) in
                return trashCanType.trashCanId == trashCanLocation.trashCanId
            }
            guard let trashCanLocation = location else {
                return nil
            }

            return TrashCan(
                trashCanId: trashCanType.trashCanId,
                type: trashCanType.type,
                cityDistrict: trashCanLocation.cityDistrict,
                street: trashCanLocation.street,
                accessibility: trashCanLocation.accessibility,
                annotation: trashCanLocation.annotation
            )
        })
        groupUpTrashCans()
    }
    
    private func groupUpTrashCans() {
        let groupedTrashCans = Dictionary(grouping: trashCans, by: { $0.trashCanId! } )
        DispatchQueue.main.async {
            self.state = .loaded
            self.trashCanGroups = groupedTrashCans.map { elem in
                let (_, value) = elem
                let distance = self.computeDistance(trashCan: value.first!)
                return TrashCanGroup(
                    trashCans: value,
                    distance: distance
                )
            }.sorted()
        }
    }
    
    private func computeDistance(trashCan: TrashCan) -> Double {
        guard let coordinates = trashCan.annotation?.coordinate else {
            return .infinity
        }
        let location = CLLocation(
            latitude: coordinates.latitude,
            longitude: coordinates.longitude
        )
        return location.distance(from: self.location) / 1000
    }
    
    private func updateTrashCanGroupsDistance() {
        DispatchQueue.main.async {
            self.trashCanGroups = self.trashCanGroups.compactMap { group in
                guard let first = group.trashCans?.first else {
                    return nil
                }
                return TrashCanGroup(
                    trashCans: group.trashCans,
                    distance: self.computeDistance(trashCan: first)
                )
            }
        }
    }
}
