//
//  TrashCanTableViewModel.swift
//  CleanPrague
//
//  Created by Adam Formanek on 02.02.2021.
//

import Foundation
import CoreLocation
import Combine

final class TrashCanTableViewModel: ObservableObject, Identifiable {
    @Published private(set) var presentingTrashCanGroups: [TrashCanGroup] = []
    @Published private(set) var state: DataState = .noData
    @Published private(set) var location: CLLocation = CLLocation(latitude: 51.507222, longitude: -0.1275)
    
    private var trashCanGroups: [TrashCanGroup] = [] {
        didSet {
            if filterApplied,
               let filter = currentFilter {
                applyFilter(filter: filter)
            } else {
                DispatchQueue.main.async {
                    self.presentingTrashCanGroups = self.trashCanGroups
                }
            }
        }
    }
    private var filterApplied = false
    private var currentFilter: FilterSelectedOptions?
    
    private var cancellables: [AnyCancellable] = []
    
    init(superModel: DashboardViewModel) {
        superModel.$trashCanGroups.sink {
            self.trashCanGroups = $0
        }.store(in: &cancellables)
        superModel.$state.sink {
            self.state = $0
        }.store(in: &cancellables)
        superModel.$location.sink {
            self.location = $0
        }.store(in: &cancellables)
    }
}

// MARK: - Filtering
extension TrashCanTableViewModel {
    func applyFilter(filter: FilterSelectedOptions) {
        filterApplied = true
        currentFilter = filter
        let maxDistance = filter.maxDistance ?? .infinity
        presentingTrashCanGroups = trashCanGroups.compactMap {
            guard $0.accessibility == filter.accessibility,
                  let distance = $0.distance,
                  distance < maxDistance,
                  let types = $0.types else {
                return nil
            }
            guard let filterTypes = filter.trashCanTypes else {
                return $0
            }
            return Set(filterTypes).isSubset(of: Set(types)) ? $0 : nil
        }.sorted()
    }
    
    func clearFilter() {
        filterApplied = false
        currentFilter = nil
        presentingTrashCanGroups = trashCanGroups
    }
}
