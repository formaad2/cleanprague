//
//  MapViewModel.swift
//  CleanPrague
//
//  Created by Adam Formanek on 01.02.2021.
//

import Foundation
import CoreLocation
import Combine
import MapKit

final class MapViewModel: ObservableObject, Identifiable {
    @Published var annotations: [AnnotationModel] = []
    
    private(set) var location: CLLocation = CLLocation(
        latitude: 51.507222,
        longitude: -0.1275
    )
    private(set) var selectedTrashCanGroup: TrashCanGroup?
    
    private var trashCanGroups: [TrashCanGroup] = []
    private var cancellables: [AnyCancellable] = []
    
    init(superModel: DashboardViewModel, selectedTrashCanGroup: TrashCanGroup? = nil) {
        self.selectedTrashCanGroup = selectedTrashCanGroup
        
        superModel.$trashCanGroups.sink {
            self.trashCanGroups = $0
        }.store(in: &cancellables)
        superModel.$location.sink {
            self.location = $0
        }.store(in: &cancellables)
    }
}

// MARK: - Distance computations
extension MapViewModel {
    func userLocationDistance(from center: CLLocationCoordinate2D) -> CLLocationDistance {
        let location = CLLocation(
            latitude: center.latitude,
            longitude: center.longitude
        )
        return location.distance(from: self.location) / 1000
    }
    
    func distance(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D) -> CLLocationDistance {
        let fromLocation = CLLocation(
            latitude: from.latitude,
            longitude: from.longitude
        )
        let toLocation = CLLocation(
            latitude: to.latitude,
            longitude: to.longitude
        )
        
        return fromLocation.distance(from: toLocation) / 1000
    }
}

// MARK: - Annotations
extension MapViewModel {
    func getCloseAnnotation(to center: CLLocation) {
        var closeAnnotations: [AnnotationModel] = self.trashCanGroups.compactMap({ group in
            let groupLocation = CLLocation(
                latitude: group.coordinates.latitude,
                longitude: group.coordinates.longitude
            )
            
            let distance = center.distance(from: groupLocation) / 1000
            
            return distance > 0.3
                ? nil
                : AnnotationModel(
                title: group.address,
                subtitle: "",
                coordinate: group.coordinates
            )
        })
        if let selectedTrashCanGroup = selectedTrashCanGroup {
            closeAnnotations.append(AnnotationModel(
                title: selectedTrashCanGroup.address,
                subtitle: "",
                coordinate: selectedTrashCanGroup.coordinates,
                isSelected: true
                )
            )
        }

        DispatchQueue.main.async {
            self.annotations = closeAnnotations
        }
    }
    
    func getOnlyCloseAnnotations() {
        var closeAnnotations: [AnnotationModel] = self.trashCanGroups.compactMap({ group in
            guard let distance = group.distance,
                  distance < 0.3 else {
                return nil
            }
            return AnnotationModel(
                title: group.address,
                subtitle: "",
                coordinate: group.coordinates
            )
        })
        if let selectedTrashCanGroup = selectedTrashCanGroup {
            closeAnnotations.append(AnnotationModel(
                title: selectedTrashCanGroup.address,
                subtitle: "",
                coordinate: selectedTrashCanGroup.coordinates,
                isSelected: true
                )
            )
        }

        DispatchQueue.main.async {
            self.annotations = closeAnnotations
        }
    }
}
