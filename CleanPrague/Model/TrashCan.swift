//
//  Trashcan.swift
//  CleanPrague
//
//  Created by Adam Formanek on 27.01.2021.
//

import Foundation
import MapKit

enum TrashType: String, Comparable, CaseIterable {
    case plast = "Plast"
    case papir = "Papír"
    case bileSklo = "Čiré sklo"
    case barevneSklo = "Barevné sklo"
    case napojoveKartony = "Nápojové kartóny"
    case kovy = "Kovy"
    case elektrozarizeni = "Elektrozařízení"
    case jinak = "Jiné"
    
    init(type: String?) {
        guard let type = type else {
            self = .jinak
            return
        }
        self = TrashType(rawValue: type) ?? .jinak
    }
    
    static func < (lhs: TrashType, rhs: TrashType) -> Bool {
        lhs.hashValue < rhs.hashValue
    }
    
}

enum AccessibilityType: String, CaseIterable {
    case volne = "volně"
    case obyvatelum = "obyvatelům domu"
    case jinak = ""
    
    init(type: String?) {
        guard let type = type else {
            self = .jinak
            return
        }
        self = AccessibilityType(rawValue: type) ?? .jinak
    }
    
    var description: String {
        "dosupný " + self.rawValue
    }
}

struct TrashCan: Identifiable {
    var id: UUID = UUID()
    let trashCanId: Int?
    let distance: String?
    let type: TrashType?
    let cityDistrict: String?
    let street: String?
    let accessibility: AccessibilityType?
    let annotation: MKPointAnnotation?
    
    init(trashBinTypeProperty: TrashBinTypeProperty? = nil,
         annotation: MKPointAnnotation? = nil
    ) {
        self.trashCanId = trashBinTypeProperty?.STATIONID
        self.type = TrashType(type: trashBinTypeProperty?.TRASHTYPENAME)
        self.annotation = annotation
        self.cityDistrict = nil
        self.street = nil
        self.accessibility = nil
        self.distance = nil
    }
    
    init(trashBinLocationProperty: TrashBinLocationProperty?,
         annotation: MKPointAnnotation? = nil
    ) {
        self.trashCanId = trashBinLocationProperty?.ID
        self.cityDistrict = trashBinLocationProperty?.CITYDISTRICT
        self.street = trashBinLocationProperty?.STATIONNAME
        self.accessibility = AccessibilityType(type: trashBinLocationProperty?.PRISTUP)
        self.annotation = annotation
        self.type = nil
        self.distance = nil
    }
    
    init(trashCanId: Int? = nil,
         distance: String? = nil,
         type: TrashType? = nil,
         cityDistrict: String? = nil,
         street: String? = nil,
         accessibility: AccessibilityType? = nil,
         annotation: MKPointAnnotation? = nil) {
        self.trashCanId = trashCanId
        self.distance = distance
        self.type = type
        self.cityDistrict = cityDistrict
        self.street = street
        self.accessibility = accessibility
        self.annotation = annotation
    }
}

// MARK: - Helper functions
extension TrashCan {
    func address() -> String {
        guard let cityDistrict = cityDistrict, let street = street else {
            return ""
        }
        return cityDistrict + ", " + street
    }
}
