//
//  FilterSelectedOptions.swift
//  CleanPrague
//
//  Created by Adam Formanek on 31.01.2021.
//

import Foundation

struct FilterSelectedOptions: Identifiable {
    let id = UUID()
    let maxDistance: Double?
    let trashCanTypes: [TrashType]?
    let accessibility: AccessibilityType?
    
    init(maxDistance: Double? = nil,
         trashCanTypes: [TrashType]? = nil,
         accessibility: AccessibilityType? = nil) {
        self.maxDistance = maxDistance
        self.trashCanTypes = trashCanTypes
        self.accessibility = accessibility
    }
}
