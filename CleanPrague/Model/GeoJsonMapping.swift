//
//  GeoJsonFormat.swift
//  CleanPrague
//
//  Created by Adam Formanek on 27.01.2021.
//

import Foundation
import MapKit

// MARK: - Trash bins
struct TrashBinTypeProperty: Decodable {
    let STATIONID: Int?
    let TRASHTYPENAME: String?
    let CONTAINERTYPE: String?
}

// MARK: - Trash bin types
struct TrashBinLocationProperty: Decodable {
    let ID: Int?
    let STATIONNAME: String?
    let CITYDISTRICT: String?
    let PRISTUP: String?
}

