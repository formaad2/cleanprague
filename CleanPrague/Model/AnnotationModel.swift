//
//  AnnotationModel.swift
//  CleanPrague
//
//  Created by Adam Formanek on 01.02.2021.
//

import Foundation
import MapKit

final class AnnotationModel: NSObject, MKAnnotation {
    let title: String?
    let subtitle: String?
    let coordinate: CLLocationCoordinate2D
    let isSelected: Bool
    
    init(title: String?,
         subtitle: String?,
         coordinate: CLLocationCoordinate2D,
         isSelected: Bool = false) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        self.isSelected = isSelected
    }
}
