//
//  TrashCanGroup.swift
//  CleanPrague
//
//  Created by Adam Formanek on 29.01.2021.
//

import Foundation
import MapKit

struct TrashCanGroup: Identifiable {
    var id: UUID = UUID()
    let trashCans: [TrashCan]?
    let distance: Double?
    
    var trashCanGroupId: Int? {
        trashCans?.first?.trashCanId
    }
    
    var address: String {
        trashCans?.first?.address() ?? ""
    }
    
    var types: [TrashType]? {
        guard let trashCans = trashCans else {
            return nil
        }
        let sortedTypes = Array(Set(trashCans.compactMap { $0.type })).sorted { $0 < $1 }
        return sortedTypes
    }
    
    var accessibility: AccessibilityType {
        trashCans?.first?.accessibility ?? .jinak
    }
    
    var coordinates: CLLocationCoordinate2D {
        trashCans?.first?.annotation?.coordinate ?? CLLocationCoordinate2D(latitude: 51.507222, longitude: -0.1275)
    }
    
    var distanceString: String {
        String(format: "%.2f", distance ?? 0) + " Km"
    }
}

extension TrashCanGroup: Comparable {
    static func < (lhs: TrashCanGroup, rhs: TrashCanGroup) -> Bool {
        guard let dist1 = lhs.distance,
              let dist2 = rhs.distance else {
            return false
        }
        return dist1 < dist2
    }
    
    static func == (lhs: TrashCanGroup, rhs: TrashCanGroup) -> Bool {
        return
            lhs.distanceString == rhs.distanceString &&
            lhs.types == rhs.types &&
            lhs.coordinates.latitude == rhs.coordinates.latitude &&
            lhs.coordinates.longitude == rhs.coordinates.longitude &&
            lhs.accessibility == rhs.accessibility &&
            lhs.trashCanGroupId == rhs.trashCanGroupId &&
            lhs.address == rhs.address
    }
}
