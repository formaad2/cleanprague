//
//  SelectableRow.swift
//  CleanPrague
//
//  Created by Adam Formanek on 31.01.2021.
//

import Foundation

struct FilterViewTrashCanType: Identifiable {
    let id = UUID()
    let trashType: TrashType
    var isSelected: Bool = false
}
