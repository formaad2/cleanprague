//
//  InfoView.swift
//  CleanPrague
//
//  Created by Adam Formanek on 29.01.2021.
//

import SwiftUI

struct InfoView: View {
    @Binding var dismiss: Bool
    var allTypes = TrashType.allCases
    
    var body: some View {
        NavigationView {
            VStack {
                Text("💁🏼‍♀️💁🏻‍♂️")
                    .lineLimit(1)
                    .font(.system(size: 70))
                    .frame(width: 200, height: 100, alignment: .center)
                    .multilineTextAlignment(.center)
                Text("Koš reprezentuje místo, kde se kontejnery nacházejí. Každý záznam obsahuje adresu, vzdálenost uživatele od místa a typy košů, které se na dané lokalitě nacházejí. \n\nU každého koše naleznete barevné značení. Každý punťík značí, jaký typ koše se na tomto stanovišti vyskytuje.")
                    .font(.subheadline)
                    .padding(.horizontal, 15)
                Form{
                    Section(header: Text("Typy kontejnerů")) {
                        ForEach(allTypes, id: \.self) { trashType in
                            HStack {
                                Text(trashType.rawValue)
                                    .font(.subheadline)
                                Spacer()
                                ColorCirclesView(types: [trashType])
                            }
                        }
                    }
                }
            }
            .navigationBarItems(
                trailing: Button(action: {dismiss.toggle()}) {
                        Text("Mám to!")
                    }
            )
            .navigationBarTitle("Info")
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

struct InfoView_Previews: PreviewProvider {
    static var previews: some View {
        StatefulPreviewWrapper(false) { InfoView(dismiss: $0) }
    }
}
