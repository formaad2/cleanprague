//
//  Dashboard.swift
//  CleanPrague
//
//  Created by Adam Formanek on 27.01.2021.
//

import SwiftUI
import MapKit

struct Dashboard: View {
    @StateObject var viewModel: DashboardViewModel = DashboardViewModel()
    @State private var centerCoordinate = CLLocationCoordinate2D()
    
    var body: some View {
        TabView {
            TrashCanTable(superViewModel: viewModel)
            .tabItem {
                Image(systemName: "trash")
                Text("Seznam")
            }
            MapContextView(superViewModel: viewModel)
            .tabItem {
                Image(systemName: "map")
                Text("Mapa")
            }
        }
    }
}

struct Dashboard_Previews: PreviewProvider {
    static var previews: some View {
        Dashboard()
    }
}

var mockTrashcans: [TrashCan] = [
    TrashCan(trashCanId: 1, type: .plast, cityDistrict: "Praha 3", street: "Nootova 12/23"),
    TrashCan(trashCanId: 1, type: .papir, cityDistrict: "Praha 3", street: "Nootova 12/23"),
    TrashCan(trashCanId: 1, type: .napojoveKartony, cityDistrict: "Praha 3", street: "Nootova 12/23")
]
