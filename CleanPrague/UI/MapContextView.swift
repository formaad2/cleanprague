//
//  MapContextView.swift
//  CleanPrague
//
//  Created by Adam Formanek on 02.02.2021.
//

import SwiftUI
import CoreLocation

struct MapContextView: View {
    var dashboardViewModel: DashboardViewModel
    @StateObject var viewModel: MapViewModel
    
    @State var mapCenterCoordinate = CLLocationCoordinate2D()
    @State var currentCenterCoordinate = CLLocationCoordinate2D()
    
    private var isUserLocationTooFar: Bool {
        viewModel.userLocationDistance(from: mapCenterCoordinate) > 0.15
    }
    
    private var isCenterLocationTooFar: Bool {
        viewModel.distance(from: mapCenterCoordinate, to: currentCenterCoordinate) > 0.15
    }
    
    init(superViewModel: DashboardViewModel, selectedTrashCanGroup: TrashCanGroup) {
        self.dashboardViewModel = superViewModel
        self._viewModel = .init(wrappedValue: MapViewModel(superModel: superViewModel, selectedTrashCanGroup: selectedTrashCanGroup))
        self.currentCenterCoordinate = selectedTrashCanGroup.coordinates
    }
    
    init(superViewModel: DashboardViewModel) {
        self.dashboardViewModel = superViewModel
        self._viewModel = .init(wrappedValue: MapViewModel(superModel: superViewModel))
    }
    
    var body: some View {
        ZStack {
            MapView(viewModel: viewModel, centerCoordinate: $mapCenterCoordinate, currentCenterCoordinate: $currentCenterCoordinate)
            if isCenterLocationTooFar {
                MapViewBottomButtonView(action: updateAnnotations)
                    .padding()
            }
        }
        .onAppear() {
            if viewModel.selectedTrashCanGroup == nil {
                viewModel.getOnlyCloseAnnotations()
            } else {
                viewModel.getCloseAnnotation(to: viewModel.selectedTrashCanGroup!.coordinates.toCLLocation())
            }
        }
    }
}

extension MapContextView {
    func updateAnnotations() {
        _currentCenterCoordinate.wrappedValue = _mapCenterCoordinate.wrappedValue
        let center = CLLocation(
            latitude: mapCenterCoordinate.latitude,
            longitude: mapCenterCoordinate.longitude
        )
        viewModel.getCloseAnnotation(to: center)
    }
}
