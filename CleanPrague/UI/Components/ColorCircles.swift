//
//  ColorCircles.swift
//  CleanPrague
//
//  Created by Adam Formanek on 29.01.2021.
//

import SwiftUI

struct ColorCirclesView: View {
    var types: [TrashType]?
    
    var body: some View {
        HStack {
            let colors = generateColors()
            ForEach(colors, id: \.self) { color in
                Circle()
                    .strokeBorder(Color.black, lineWidth: 1)
                    .background(Circle().foregroundColor(color))
                    .frame(width: 20, height: 20, alignment: .leading)
            }
        }
    }
}

struct ColorCircles_Previews: PreviewProvider {
    static var previews: some View {
        ColorCirclesView(types: [.plast, .papir, .napojoveKartony])
    }
}

extension ColorCirclesView {
    func generateColors() -> [Color] {
        guard let types = types else {
            return []
        }
        return types.compactMap { (trashType) in
            switch trashType {
            case .plast: return .yellow
            case .papir: return .blue
            case .bileSklo: return .white
            case .barevneSklo: return .green
            case .napojoveKartony: return .orange
            case .kovy: return .gray
            case .elektrozarizeni: return .red
            default: return .black
            }
        }
    }
}
