//
//  MultipleSelectionRow.swift
//  CleanPrague
//
//  Created by Adam Formanek on 31.01.2021.
//

import SwiftUI

struct SelectableTrashTypeView: View {
    @Binding var content: FilterViewTrashCanType
        
    var body: some View {
        Button(action: { content.isSelected.toggle() }) {
            HStack {
                Text(content.trashType.rawValue)
                Spacer()
                Image(systemName: content.isSelected ? "checkmark.circle.fill" : "circle")
            }
        }
        .font(.subheadline)
        .buttonStyle(PlainButtonStyle())
    }
}

struct SelectableTrashTypeView_Previews: PreviewProvider {
    static var previews: some View {
        StatefulPreviewWrapper(FilterViewTrashCanType(trashType: .papir)) {
            SelectableTrashTypeView( content: $0 )
        }
    }
}
