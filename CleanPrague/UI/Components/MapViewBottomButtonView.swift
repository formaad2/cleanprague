//
//  MapViewBottomButtonView.swift
//  CleanPrague
//
//  Created by Adam Formanek on 02.02.2021.
//

import SwiftUI

struct MapViewBottomButtonView: View {
    var action: () -> Void
        
    var body: some View {
        VStack {
            Spacer()
            ZStack {
                RoundedRectangle(cornerRadius: 10, style: .circular)
                    .foregroundColor(.accentColor)
                Button(action: action) {
                    Text("Načíst kontejnery poblíž")
                        .foregroundColor(.white)
                        .font(.subheadline)
                        .bold()
                }
            }
            .padding()
            .frame(width: 250, height: 70, alignment: .center)
        }
    }
}

struct MapViewBottomButtonView_Previews: PreviewProvider {
    static var previews: some View {
        MapViewBottomButtonView(action: {})
    }
}
