//
//  NoTrashCansVIew.swift
//  CleanPrague
//
//  Created by Adam Formanek on 01.02.2021.
//

import SwiftUI

struct NoTrashCansView: View {
    var body: some View {
        VStack {
            Text("Bohužel, žádné koše jsem nenašel...")
                .font(.subheadline)
                .padding(.horizontal, 30)
                .multilineTextAlignment(.center)
            Image(systemName: "trash.slash")
                .resizable()
                .frame(width: 50, height: 50, alignment: .center)
                .accentColor(.accentColor)
        }
    }
}

struct NoTrashCansView_Previews: PreviewProvider {
    static var previews: some View {
        NoTrashCansView()
    }
}
