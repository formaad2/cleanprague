//
//  ActivityIndicator.swift
//  CleanPrague
//
//  Created by Adam Formanek on 28.01.2021.
//

import SwiftUI
import Foundation

struct ActivityIndicatorView: UIViewRepresentable {
    var animate: Bool
    
    func makeUIView(context: Context) -> some UIActivityIndicatorView {
        let indicator = UIActivityIndicatorView(style: .medium)
        return indicator
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
        animate ? uiView.startAnimating() : uiView.stopAnimating()
    }
}
