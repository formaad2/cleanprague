//
//  TrashCanTableCell.swift
//  CleanPrague
//
//  Created by Adam Formanek on 28.01.2021.
//

import SwiftUI

struct TrashCanTableCell: View {
    var trashCanGroup: TrashCanGroup
    
    var body: some View {
        HStack{
            Image(systemName: "trash")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 45, height: 50, alignment: .leading)
                .padding(.trailing, 15)
            VStack(alignment: .leading){
                ColorCirclesView(types: trashCanGroup.types)
                Text(trashCanGroup.address)
                    .font(.subheadline)
                Text(trashCanGroup.distanceString)
                    .font(.subheadline)
                Text(trashCanGroup.accessibility.description)
                    .font(.subheadline)
            }
        }
        .padding(EdgeInsets(top: 5, leading: 0, bottom: 5, trailing: 0))
        .frame(maxWidth: .infinity, maxHeight: 90, alignment: .leading)
    }
}

struct TrashCanTableCell_Previews: PreviewProvider {
    static var previews: some View {
        TrashCanTableCell(trashCanGroup: TrashCanTableCell.preview)
    }
}

extension TrashCanTableCell {
    static var preview = TrashCanGroup(
        trashCans:
            [TrashCan(trashCanId: 1, type: .plast, cityDistrict: "Praha 3", street: "Nootova 12/23"),
             TrashCan(trashCanId: 1, type: .papir, cityDistrict: "Praha 3", street: "Nootova 12/23"),
             TrashCan(trashCanId: 1, type: .napojoveKartony, cityDistrict: "Praha 3", street: "Nootova 12/23")
            ],
        distance: nil
    )
}
