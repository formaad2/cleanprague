//
//  TrashCanTable.swift
//  CleanPrague
//
//  Created by Adam Formanek on 28.01.2021.
//

import SwiftUI

struct TrashCanTable: View {
    var dashboardViewModel: DashboardViewModel
    @StateObject var viewModel: TrashCanTableViewModel
    
    @State private var isShowingInfo = false
    @State private var isShowingFilter = false
    @State private var isFilterApplied = false
    @State private var filter = FilterSelectedOptions()
    
    init(superViewModel: DashboardViewModel) {
        self.dashboardViewModel = superViewModel
        self._viewModel = .init(wrappedValue: TrashCanTableViewModel(superModel: superViewModel))
    }
    
    var loading: Bool {
        viewModel.state != .loaded
    }
    
    var body: some View {
        NavigationView{
            ZStack {
                if !loading {
                    if viewModel.presentingTrashCanGroups.isEmpty && isFilterApplied {
                        NoTrashCansView()
                    } else {
                        List(viewModel.presentingTrashCanGroups){
                            trashCanGroup in NavigationLink(
                                destination: MapContextView(superViewModel: dashboardViewModel, selectedTrashCanGroup: trashCanGroup)
                                    .navigationTitle(Text(trashCanGroup.address))
                                    .navigationBarTitleDisplayMode(.inline)
                            ) {
                                TrashCanTableCell(trashCanGroup: trashCanGroup)
                                    .padding(.vertical, 5)
                            }
                        }
                    }
                }
                ActivityIndicatorView(animate: loading)
                    .frame(width: 50, height: 50, alignment: .center)
            }
            .navigationTitle("Seznam popelnic")
            .navigationBarItems(
                leading: Button(action: loadData) {
                    Image(systemName: "arrow.clockwise.circle")
                        .font(.title)
                }
                .disabled(loading),
                trailing: HStack{
                    Button(action: { self.isShowingInfo.toggle() } ) {
                    Image(systemName: "info.circle")
                        .font(.title)
                    }
                    .sheet(isPresented: $isShowingInfo, content: {
                        InfoView(dismiss: $isShowingInfo)
                    })
                    Button(action: { self.isShowingFilter.toggle() }) {
                        Image(systemName: isFilterApplied ? "line.horizontal.3.decrease.circle.fill" : "line.horizontal.3.decrease.circle")
                        .font(.title)
                    }
                    .disabled(loading)
                    .sheet(isPresented: $isShowingFilter, content: {
                        FilterView(dismiss: $isShowingFilter, previousFilter: self.filter, filterFunc: { filter in
                            self.isFilterApplied = true
                            self.filter = filter
                            viewModel.applyFilter(filter: self.filter)
                        }) {
                            self.isFilterApplied = false
                            self.filter = FilterSelectedOptions()
                            viewModel.clearFilter()
                        }
                    })
                }
            )
        }
        .onAppear {
            if loading {
                loadData()
            }
        }
    }
}

extension TrashCanTable {
    func loadData() {
        dashboardViewModel.fetchTrashBins()
    }
}

struct TrashCanTable_Previews: PreviewProvider {
    static var previews: some View {
        TrashCanTable(superViewModel: DashboardViewModel())
    }
}


