//
//  Map.swift
//  CleanPrague
//
//  Created by Adam Formanek on 27.01.2021.
//

import SwiftUI
import MapKit

enum AnnotationViewIdetifier: String {
    case user = "UserPlacemark"
    case trashCan = "TrashCanPlacemark"
    case selectedTrashCan = "SelectedTrashCanPlacemark"
}

struct MapView: UIViewRepresentable {
    @ObservedObject var viewModel: MapViewModel
    @Binding var centerCoordinate: CLLocationCoordinate2D
    @Binding var currentCenterCoordinate: CLLocationCoordinate2D
    
    private var annotations: [MKAnnotation] {
        viewModel.annotations
    }
    
    func makeUIView(context: Context) -> MKMapView {
        let mapView = MKMapView(frame: .zero)
        mapView.delegate = context.coordinator
        mapView.showsUserLocation = true
        mapView.setRegion(getCenterRegion(), animated: true)
        mapView.addAnnotations(annotations)
        return mapView
    }
    
    func updateUIView(_ view: MKMapView, context: Context) {
        let pointAnnotations = annotations + [view.userLocation]
        if !(pointAnnotations == view.annotations) {
            view.removeAnnotations(view.annotations)
            view.addAnnotations(pointAnnotations)
        }
    }
    
    func makeCoordinator() -> MapViewCoordinator{
         MapViewCoordinator(self)
    }
    
    class MapViewCoordinator: NSObject, MKMapViewDelegate {
        var parent: MapView
        
        init(_ control: MapView) {
            self.parent = control
        }
        
        /// this method is called after user stops moving with map
        func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
            if parent.centerCoordinate == CLLocationCoordinate2D() {
                parent.centerCoordinate = mapView.centerCoordinate
                parent.currentCenterCoordinate = mapView.centerCoordinate
            } else {
                parent.centerCoordinate = mapView.centerCoordinate
            }
        }
        
        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            
            var identifier: AnnotationViewIdetifier
            switch annotation {
            case is MKUserLocation:
                identifier = .user
            default:
                if (annotation as! AnnotationModel).isSelected {
                    identifier = .selectedTrashCan
                } else {
                    identifier = .trashCan
                }
            }
            
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier.rawValue)
            
            if identifier == .user {
                annotationView = userAnnotationView(mapView, annotation: annotation)
            } else if identifier == .selectedTrashCan {
                annotationView = selectedTrashCanGroupAnnotationView(mapView, annotation: annotation)
            } else {
                annotationView = trashCanGroupAnnotationView(mapView, annotation: annotation)
            }
            
            return annotationView
        }
        
        private func userAnnotationView(_ mapView: MKMapView, annotation: MKAnnotation) -> MKAnnotationView? {
            let identifier: AnnotationViewIdetifier = .user
            let annotationView = MKUserLocationView(annotation: annotation, reuseIdentifier: identifier.rawValue)
            return annotationView
        }
        
        private func trashCanGroupAnnotationView(_ mapView: MKMapView, annotation: MKAnnotation) -> MKAnnotationView? {
            let identifier: AnnotationViewIdetifier = .trashCan
            let annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier.rawValue)
        
            annotationView.canShowCallout = true
        
            return annotationView
        }
        
        private func selectedTrashCanGroupAnnotationView(_ mapView: MKMapView, annotation: MKAnnotation) -> MKAnnotationView? {
            let identifier: AnnotationViewIdetifier = .selectedTrashCan
            let annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier.rawValue)
            
            annotationView.isHighlighted = true
            annotationView.glyphImage = UIImage(systemName: "trash")
            annotationView.markerTintColor = .blue
            annotationView.canShowCallout = true
            annotationView.displayPriority = .required

            return annotationView
        }
    }
}

extension MapView {
    func getCenterRegion() -> MKCoordinateRegion {
        let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
        let center = CLLocationCoordinate2D(
            latitude: viewModel.selectedTrashCanGroup?.coordinates.latitude ?? viewModel.location.coordinate.latitude,
            longitude: viewModel.selectedTrashCanGroup?.coordinates.longitude ?? viewModel.location.coordinate.longitude
            )
        let region = MKCoordinateRegion(center: center, span: span)
        return region
    }
}
