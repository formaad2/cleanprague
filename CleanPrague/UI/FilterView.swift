//
//  FilterView.swift
//  CleanPrague
//
//  Created by Adam Formanek on 31.01.2021.
//

import SwiftUI

struct FilterView: View {
    @Binding var dismiss: Bool
    let filterFunc: (FilterSelectedOptions) -> Void
    let clearFilterFunc: () -> Void
    let previousFilter: FilterSelectedOptions
    
    @State private var selectedAccessibility: AccessibilityType
    @State private var distanceText = ""
    @State private var trashCanTypes: [FilterViewTrashCanType]
    
    private var trashCanAccessibilities: [AccessibilityType]
    
    init(dismiss: Binding<Bool>, previousFilter: FilterSelectedOptions, filterFunc: @escaping (FilterSelectedOptions) -> Void, clearFilterFunc: @escaping () -> Void) {
        self._dismiss = dismiss
        self.filterFunc = filterFunc
        self.clearFilterFunc = clearFilterFunc
        
        self.previousFilter = previousFilter
        
        self._trashCanTypes = .init(initialValue: TrashType.allCases.map { FilterViewTrashCanType(
            trashType: $0,
            isSelected: previousFilter.trashCanTypes?.contains($0) ?? false)
        })
        self._distanceText = .init(initialValue: previousFilter.maxDistance == nil ? "" : String(previousFilter.maxDistance!))
        self._selectedAccessibility = .init(initialValue: previousFilter.accessibility ?? .volne)
        self.trashCanAccessibilities = AccessibilityType.allCases.filter { $0 != .jinak }
    }
    
    var body: some View {
        NavigationView {
            VStack {
                Form {
                    Section(header: Text("Zvolte maximální vzdálenost ke kontejneru")) {
                        TextField("Minimální vzdálenost", text: $distanceText)
                            .keyboardType(.numberPad)
                    }
                    Section(header: Text("Dostupnost kontejnerů")) {
                        Picker(selection: $selectedAccessibility, label: Text("Vyberte typ dostupnosti")) {
                            ForEach(trashCanAccessibilities, id: \.self) { access in
                                if access != .jinak {
                                    Text(access.rawValue)
                                }
                            }
                        }
                        .pickerStyle(SegmentedPickerStyle())
                    }
                    Section(header: Text("Typ kontejneru")) {
                        ForEach(0..<trashCanTypes.count) {
                            SelectableTrashTypeView(content: $trashCanTypes[$0])
                        }
                    }
                }
                HStack {
                    Button(action: clearTrashCanFilter) {
                        ZStack {
                            RoundedRectangle(cornerRadius: 10, style: .continuous)
                                .foregroundColor(.accentColor)
                            RoundedRectangle(cornerRadius: 10, style: .continuous)
                                .padding(3)
                                .foregroundColor(.white)
                            Text("Vyčistit")
                                .foregroundColor(.black)
                                .font(.title3)
                        }
                        .frame(width: 150, height: 50, alignment: .center)
                    }
                    Button(action: filterTrashCanGroups) {
                        ZStack {
                            RoundedRectangle(cornerRadius: 10.0, style: .continuous)
                                .foregroundColor(.accentColor)
                                .frame(width: 150, height: 50, alignment: .center)
                            Text("Filtruj")
                                .foregroundColor(.white)
                                .font(.title3)
                        }
                    }
                }
            }
            .navigationBarItems(
                leading: Button(action: {dismiss.toggle()}) {
                        Text("Zrušit")
                }
            )
            .navigationBarTitle("Filtr košů")
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

extension FilterView {
    func filterTrashCanGroups() {
        filterFunc(FilterSelectedOptions(
            maxDistance: Double(self.distanceText),
            trashCanTypes: trashCanTypes.compactMap { item in
                if item.isSelected == true { return item.trashType }
                else { return nil }
            },
            accessibility: selectedAccessibility
        ))
        dismiss.toggle()
    }
    
    func clearTrashCanFilter() {
        clearFilterFunc()
        dismiss.toggle()
    }
}
