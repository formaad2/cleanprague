//
//  Utility.swift
//  CleanPrague
//
//  Created by Adam Formanek on 29.01.2021.
//


// SOURCE: https://github.com/AlanQuatermain/AQUI/blob/master/Sources/AQUI/StatefulPreviewWrapper.swift

import SwiftUI
import CoreLocation
import MapKit

@available(macOS 10.15, iOS 13, tvOS 13, watchOS 6, *)
public struct StatefulPreviewWrapper<Value, Content: View>: View {
    @State var value: Value
    var content: (Binding<Value>) -> Content

    public var body: some View {
        content($value)
    }

    public init(_ value: Value, content: @escaping (Binding<Value>) -> Content) {
        self._value = State(wrappedValue: value)
        self.content = content
    }
}

extension CLLocationCoordinate2D {
    static func ==(lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
        return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
    }
}

extension Array where Element == MKAnnotation {
    static func ==(lhs: [MKAnnotation], rhs: [MKAnnotation]) -> Bool {
        guard lhs.count == rhs.count else {
            return false
        }
    
        for elem in lhs {
            if rhs.first(where: {
                $0.coordinate == elem.coordinate
            }) == nil {
                return false
            }
        }
        
        return true
    }
}

extension CLLocationCoordinate2D {
    func toCLLocation() -> CLLocation {
        let lat = self.latitude
        let lon = self.longitude
        return CLLocation(latitude: lat, longitude: lon)
    }
}
