//
//  Networking.swift
//  CleanPrague
//
//  Created by Adam Formanek on 27.01.2021.
//

import Foundation
import Combine

enum URLs: String {
    case types = "https://opendata.iprpraha.cz/CUR/ZPK/ZPK_O_Kont_TOitem_b/WGS_84/ZPK_O_Kont_TOitem_b.json"
    case locations = "https://opendata.iprpraha.cz/CUR/ZPK/ZPK_O_Kont_TOstan_b/WGS_84/ZPK_O_Kont_TOstan_b.json"
}

protocol NetworkServicing {
    func fetch(url: URLs, completion: @escaping (Result<Data, Error>) -> Void)
    func fetch(urlRequest: URLRequest, completion: @escaping (Result<Data, Error>) -> Void)
}

struct NetworkService: NetworkServicing {
    func fetch(url: URLs, completion: @escaping (Result<Data, Error>) -> Void) {
        let urlRequest = URLRequest(url: URL(string: url.rawValue)!)
        fetch(urlRequest: urlRequest, completion: completion)
    }
    
    func fetch(urlRequest: URLRequest, completion: @escaping (Result<Data, Error>) -> Void) {
        URLSession.shared
            .dataTask(with: urlRequest, completionHandler: { data, response, error in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                
                guard let data = data else { assertionFailure(); return }
                
                completion(.success(data))
            })
            .resume()
    }
}
