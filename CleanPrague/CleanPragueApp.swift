//
//  CleanPragueApp.swift
//  CleanPrague
//
//  Created by Adam Formanek on 27.01.2021.
//

import SwiftUI

@main
struct CleanPragueApp: App {
    var body: some Scene {
        WindowGroup {
            Dashboard()
        }
    }
}
